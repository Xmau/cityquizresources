package cat.itb.cityquiz.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cat.itb.cityquiz.R;

import static androidx.navigation.Navigation.findNavController;

public class GameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);
        //Daerrorestamierda
        //NavigationUI.setupActionBarWithNavController(this,findNavController(this, R.id.nav_graph));

    }
    @Override
    public boolean onSupportNavigateUp() {
        // Afegir si es vol mostrar el 'back' a la actionBar
        return findNavController(this, R.id.nav_graph).navigateUp();
    }
}

package cat.itb.cityquiz.presentation;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;



import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.AnswerCountDownTimer;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

import static cat.itb.cityquiz.domain.Game.MAX_MILIS_PER_ANSWER;


public class GameViewModel extends ViewModel {
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    AnswerCountDownTimer answerCountDownTimer = new AnswerCountDownTimer(MAX_MILIS_PER_ANSWER);
    MutableLiveData<Game> game = new MutableLiveData<>();


    public void startQuiz(){
        game.postValue(gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers));
    }

    public LiveData<Game> getGame(){
        return game;
    }

    public void answerQuestion(int answer){
        game.postValue(gameLogic.answerQuestions(game.getValue(), answer));
    }

    public int getNumCorrectAnswers(){
        return game.getValue().getNumCorrectAnswers();
    }
    public int getScore(){
        return game.getValue().getScore();
    }

    public void skipQuestion(){
        game.postValue(gameLogic.skipQuestion(game.getValue()));
    }

    public void stop(){
        answerCountDownTimer.stop();
    }

    public void start(){
        answerCountDownTimer.start();
    }


    public void setTimerChangedListener(AnswerCountDownTimer.TimerChangedListener timerChangedListener) {
        answerCountDownTimer.setTimerChangedListener(timerChangedListener);
    }
}

package cat.itb.cityquiz.presentation.Start;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;

import cat.itb.cityquiz.presentation.GameViewModel;


public class StartFragment extends Fragment {

    private GameViewModel startViewModel;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        startViewModel = ViewModelProviders.of(getActivity()).get(GameViewModel.class);
        // TODO: Use the ViewModel


    }

    @OnClick(R.id.start_button)
    public void onViewClicked(View view) {
        startViewModel.startQuiz();
        Navigation.findNavController(view).navigate(R.id.startQuiz);
    }
}

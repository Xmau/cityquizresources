package cat.itb.cityquiz.presentation.Quiz;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;


import butterknife.BindView;
import butterknife.ButterKnife;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.GameViewModel;
import cat.itb.cityquiz.presentation.Start.StartFragment;
import cat.itb.cityquiz.repository.AnswerCountDownTimer;
import cat.itb.cityquiz.repository.GameLogic;


public class QuizFragment extends Fragment {
    private GameViewModel quizViewModel;
    @BindView(R.id.button)
    Button button1;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.button4)
    Button button4;
    @BindView(R.id.button5)
    Button button5;
    @BindView(R.id.button6)
    Button button6;

    @BindView(R.id.ImageQuiz)
    ImageView imageQuiz;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    AnswerCountDownTimer answerCountDownTimer = new AnswerCountDownTimer(5000);

    AnswerCountDownTimer.TimerChangedListener timerChangedListener;




    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.quiz_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(GameViewModel.class);
        // TODO: Use the ViewModel
        quizViewModel.getGame().observe(this,this::onChanged);

    }

    private void onChanged(Game game) {
        if(game.isFinished()){
            Navigation.findNavController(getView()).navigate(R.id.resultFragment);
            quizViewModel.stop();
        } else {
            display(game);
            quizViewModel.start();
            quizViewModel.setTimerChangedListener(this::onTimeChanged);
        }
    }

    private void onTimeChanged(int i) {
        progressBar.setProgress(i);
        if(i==0) quizViewModel.skipQuestion();
    }


    private void display(Game game) {

        String cityname = game.getCurrentQuestion().getCorrectCity().getName();
        String fileName = ImagesDownloader.scapeName(cityname);

        int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        imageQuiz.setImageResource(resId);

        answerButton(game,button1,0);
        answerButton(game,button2,1);
        answerButton(game,button3,2);
        answerButton(game,button4,3);
        answerButton(game,button5,4);
        answerButton(game,button6,5);



    }

    private void answerButton(Game game, Button button, int i) {
        button.setText(game.getCurrentQuestion().getPossibleCities().get(i).getName());
        button.setTag(i);

        button.setOnClickListener(this::answerQuestion);
    }

    public void answerQuestion(View view) {
        int answer = (Integer) view.getTag();
        quizViewModel.answerQuestion(answer);
    }
}
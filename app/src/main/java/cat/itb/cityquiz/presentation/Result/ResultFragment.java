package cat.itb.cityquiz.presentation.Result;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.presentation.GameViewModel;
import cat.itb.cityquiz.presentation.Start.StartFragment;

public class ResultFragment extends Fragment {
    private GameViewModel resultViewModel;
    //Button button;
    @BindView(R.id.score)
    TextView score;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.result_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        //button = view.findViewById(R.id.playAgain);
        //button.setOnClickListener(this::onViewClicked);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        resultViewModel = ViewModelProviders.of(getActivity()).get(GameViewModel.class);
        // TODO: Use the ViewModel


        score.setText("" + resultViewModel.getScore());


    }

    @OnClick(R.id.playAgain)
    public void onViewClicked(View view) {
        resultViewModel.startQuiz();
        Navigation.findNavController(view).navigate(R.id.actionPlayAgain);
    }

}

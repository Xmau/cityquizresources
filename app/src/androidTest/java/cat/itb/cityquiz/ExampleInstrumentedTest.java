package cat.itb.cityquiz;

import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;

import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.cityquiz.presentation.GameActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)

public class ExampleInstrumentedTest {


    @Rule
    public ActivityTestRule<GameActivity> mActivityTestRule = new ActivityTestRule<>(GameActivity.class);

    @Test
    public void testNavigation() {
        onView(withId(R.id.start_button))
                .perform(click());
        onView(withId(R.id.ImageQuiz))
                .check(matches(isDisplayed()));

        onView(withId(R.id.button))
                .perform(click());
        onView(withId(R.id.button2))
                .perform(click());
        onView(withId(R.id.button3))
                .perform(click());
        onView(withId(R.id.button4))
                .perform(click());
        onView(withId(R.id.button5))
                .perform(click());
        onView(withId(R.id.score))
                .check(matches(isDisplayed()));

        onView(withId(R.id.playAgain))
                .perform(click());
        onView(withId(R.id.ImageQuiz))
                .check(matches(isDisplayed()));

    }
}
